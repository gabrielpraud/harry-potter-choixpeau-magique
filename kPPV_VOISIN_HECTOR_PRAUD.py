#coding = utf8
"""
projet le choixpeau magique

Auteur : Clément Voisn, Marck Hector, Gabriel Praud

Date: 11/13/22
"""
import csv
import math

#ouverture du fichier 1  et indexation en fonction du prénom de clef Name car cest le descripteur commun aux 2 fichiers

with open("Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    caracters = [{key : value for key, value in element.items()} for element in reader]
index_characters = {}
for element in caracters:
    if element['Name'] in index_characters:
        index_characters[element['Name']].update({'Gender': element['Gender'],
         'Job': element['Job'], 'House': element['House'], 'Wand': element['Wand'],
         'Patronus': element['Patronus'], 'Species': element['Species'],
         'Blood status': element['Blood status'], 'Hair colour': element['Hair colour'],
         'Loyalty': element['Loyalty'], 'Eye colour': element['Eye colour'],
         'Skills': element['Skills'], 'Birth': element['Birth'], 'Death': element['Death']})
    else:
        index_characters[element['Name']] = {'Gender': element['Gender'], 'Job': element['Job'],
         'House': element['House'], 'Wand': element['Wand'], 'Patronus': element['Patronus'],
         'Species': element['Species'], 'Blood status': element['Blood status'],
         'Hair colour': element['Hair colour'], 'Loyalty': element['Loyalty'],
         'Eye colour': element['Eye colour'], 'Skills': element['Skills'],
         'Birth': element['Birth'], 'Death': element['Death']}

# ouverture du fichier 2  et indexation en fonction du prénom de clef Name car c'est le descripteur commun aux 2 fichiers

with open("Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    caracters2 = [{key : value for key, value in element.items()} for element in reader]
    liste_persos = []
    
index_characters2 = {}
for element in caracters2:
    if element['Name'] in index_characters2:
        index_characters2[element['Name']].update({'Courage': element['Courage'],
        'Ambition': element['Ambition'],'Intelligence': element['Intelligence'],'Good': element['Good']})
    else:
        index_characters2[element['Name']] = {'Courage': element['Courage'],
        'Ambition': element['Ambition'],'Intelligence': element['Intelligence'],'Good': element['Good']}

#fusion des deux index (index_caracters, index_carcaters2 ) en fonction de leurs descripteurs commun 

index_2_caracters = {}
for prenom, caracteristiques in index_characters.items():
    for prenom2, caracteristiques2 in index_characters2.items():
        if prenom == prenom2:
            index_2_caracters[prenom] = {'fichier1' : caracteristiques,
                                                'fichier2' : caracteristiques2}

# mise en forme des données demandées sous forme de liste de liste
liste_profil_type = [[9, 2, 8, 9],
                     [6, 7, 9, 7],
                     [3, 8, 6, 3],
                     [2, 3, 7, 8],
                     [3, 4, 8, 8]]

def profil_demande(indice, liste):
    '''
    fonction qui permet de prendre chaque valeur d'une liste 
    ENTREE = entier,liste
    SORTIE = liste
    '''
    valeur = liste[indice]
    return valeur


def donnees_personnages(index):
    '''
    fonction qui permet de rendre sous forme d'entier les valeurs de index indexé dans la bon ordre
    ENTREE : liste de dictionnaires 'index'
    SORTIE : liste de liste 'liste_donnees'  
    '''
    liste_donnees = []
    for element in index :
        liste_dans_liste = [int(float(element["Courage"])),int(float(element["Ambition"])),
        int(float(element["Intelligence"])), int(float(element["Good"]))]
        liste_donnees.append(liste_dans_liste)
    return liste_donnees

def distance_euclidienne(point1, point2):
    ''' fonction permetant de definir une distance entre deux points
        ENTREE: 2 variables (point 1, point 2) 
        SORTIE: math.sqrt(distance_carre)
    '''
    distance_carre = 0
    for i in range(len(point1)):
        distance_carre = distance_carre + (point1[i] - point2[i]) ** 2
    return math.sqrt(distance_carre)


def kkpv(donnees,profil, k):
    '''fonction permettant de determiner les plus proches voisins d'un profil type
        ENTREE: liste de liste : 'données', liste : 'profil', et un entier 'k'
        SORTIE : liste : 'indices_des_k_voisins'  contenant les indices des 'k' plus proches voisins
    '''

    distances_voisins = []
    for index, sample in enumerate(donnees):
        distance = distance_euclidienne(sample, profil)
        distances_voisins.append((distance, index))
    distances_voisins = sorted(distances_voisins)
    indices_des_k_voisins = [index for distance, index in distances_voisins[:k]]
    return indices_des_k_voisins


def choix_maison(liste_k_voisins, index, profil):
    '''
    fonction permettant un afichage clair des plus proches voisins d'un profil type 
    ENTREE : 2 listes et 1 dictionnaire de dictionaires
    SORTIE : Affichage des valeurs du profil type en fonction de la liste 'profil'
            Affichage des prenoms et maisons  en prenant comme indice les element de 'liste_k_voisins'
            Affichage de la maison choisit en fonction ds kppv
    '''
    print(f"\nle profil recherché à un courage de {profil[0]} une Ambition de {profil[1]}"
     "une Intelligence de {profil[2]} une 'bonté' de {profil[3]}")
    list_houses = []
    liste_choix = [0, 0, 0, 0]
    liste_choix_nom = ['Gryffindor', 'Slytherin', 'Ravenclaw', 'Hufflepuff']
    for element in index_2_caracters:
        list_houses.append(index_2_caracters[element]['fichier1']['House'])
    perso = []
    for element in index:
        perso.append(element)
    for element in liste_k_voisins:
        print(f"il ya le personnage {perso[element]} de la maison {list_houses[element]}"
         "qui est dans les plus proches voisins du profil type")
    # mise du nombre d'entier des kppv selectioné dans liste_choix indexé en fonction de leur maison
    for element in liste_k_voisins:
        if list_houses[element] == 'Gryffindor':
            liste_choix[0] += 1
        elif list_houses[element] == 'Slytherin':
            liste_choix[1] += 1
        elif list_houses[element] == 'Ravenclaw':
            liste_choix[2] += 1
        else :
            liste_choix[3] += 1
    #trie des entier de liste_choix du plus grand au plus petit pour faire de même avec la liste des noms de maison 
    # pour avoir la maison choisit en indice 0 
    for i in range(len(liste_choix) - 1):
        indice_du_max = i
        for j in range(i + 1, len(liste_choix)) :
            if liste_choix[j] > liste_choix[indice_du_max]:
                indice_du_max = j
        liste_choix[i], liste_choix[indice_du_max] = liste_choix[indice_du_max], liste_choix[i]
        liste_choix_nom[i], liste_choix_nom[indice_du_max] = liste_choix_nom[indice_du_max], liste_choix_nom[i]
    print(f" Vous appartenez à la maison {liste_choix_nom[0]}")
    return

def choix_maison2(liste_k_voisins, index, profil):
    '''
    fonction permettant un afichage clair des plus proches voisins d'un profil type 
    ENTREE : 2 listes et 1 dictionnaire de dictionaires
    SORTIE : Affichage des valeurs du profil type en fonction de la liste 'profil'
            Affichage des prenoms et maisons  en prenant comme indice les element de 'liste_k_voisins'
            Affichage de la maison choisie en fonction des kppv
    '''
    print(f"\nle profil recherché à un courage de {profil[0]} une Ambition de {profil[1]}"
     "une Intelligence de {profil[2]} une 'bonté' de {profil[3]}")
    list_houses = []
    liste_choix = [0, 0, 0, 0]
    liste_choix_nom = ['Gryffindor', 'Slytherin', 'Ravenclaw', 'Hufflepuff']
    for element in index_2_caracters:
        list_houses.append(index_2_caracters[element]['fichier1']['House'])
    perso = []
    for element in index:
        perso.append(element)
    
    for element in liste_k_voisins:
        print(f"il ya le personnage {perso[element]} de la maison {list_houses[element]}"
         "qui est dans les plus proches voisins du profil type")
    for element in liste_k_voisins:
        if list_houses[element] == 'Gryffindor':
            liste_choix[0] += 1
        elif list_houses[element] == 'Slytherin':
            liste_choix[1] += 1
        elif list_houses[element] == 'Ravenclaw':
            liste_choix[2] += 1
        else :
            liste_choix[3] += 1
    for i in range(len(liste_choix) - 1):
        indice_du_max = i
        for j in range(i + 1, len(liste_choix)) :
            if liste_choix[j] > liste_choix[indice_du_max]:
                indice_du_max = j
        liste_choix[i], liste_choix[indice_du_max] = liste_choix[indice_du_max], liste_choix[i]
        liste_choix_nom[i], liste_choix_nom[indice_du_max] = liste_choix_nom[indice_du_max], liste_choix_nom[i]
    print(f" Ce profil appartient à la maison {liste_choix_nom[0]}")

def ihm() :
    '''
    fonction qui permet de faire marcher les fonctions précédement défini entre elles
    soit avec 5 profil choisit ou un profil aléatoire 
    ENTREE : fonctions : 'choix_maison','choix_maison2', 'kkpv', 'donnes_personnages', 'profil_type' 
            entier : nb_voisins 
            dictionaires de dictionaires : 'index_2_caracters'
    SORTIE : None
    '''
    choix = int(input(f" si vous voulez tester 5 profils prédéfini entrez '1', "
    "si vous voulez créer votre propre profil entrez '2' ou si vous avez fini de tester le programme entrez '3' : "))
    assert choix == 1 or choix == 2 or choix == 3 , "vous n'avez pas saisi un nombre correct "
    if choix == 1 :
        for i in range(5):
            indice_des_k_voisins_demande = kkpv(donnees_personnages(caracters2),profil_demande(i,liste_profil_type),5)
            choix_maison2(indice_des_k_voisins_demande,index_2_caracters,profil_demande(i,liste_profil_type))
        ihm()
    elif choix == 2 :
        nb_voisins  = int(input("\ncombien de plus proches voisins voulez vous tester ? (sachant que le maximum est 20) : "))
        assert nb_voisins != 0 and nb_voisins <= 20 , " le nombre de K plus proches voisins saisit est incorrect "
        liste_données_profil = []
        print(" vous devez definir votre profil avec des valeurs d'entier de 0 à 9 ")
        courage = int(input('quel est la valeur de votre Courage : '))
        assert courage >= 0 and courage <= 9, " la valeur de votre courage saisi est incorrect "
        ambition = int(input('quel est la valeur de votre Ambiton : '))
        assert ambition >= 0 and ambition <= 9, " la valeur de votre ambition saisi est incorrect "
        intelligence = int(input('quel est la valeur de votre Intelligence : '))
        assert intelligence >= 0 and intelligence <= 9, " la valeur de votre intelligence saisi est incorrect "
        bontee = int(input("quel est la valeur de votre 'bonté' : "))
        assert bontee >= 0 and bontee <= 9, " la valeur de votre bontée saisi est incorrect "
        liste_données_profil = [courage, ambition, intelligence, bontee]
        indice_k_voisins = kkpv(donnees_personnages(caracters2), liste_données_profil, nb_voisins)
        choix_maison(indice_k_voisins, index_2_caracters, liste_données_profil)
        ihm()
    elif choix == 3 :
        print("merci d'avoir essayé notre programme de recherche. ")
ihm()
